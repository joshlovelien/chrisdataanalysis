if(!require(doParallel)) install.packages("doParallel")
if(!require(foreach)) install.packages("foreach")
if(!require(ff)) install.packages("ff")
if(!require(stringr)) install.packages("stringr")
if(!require(poibin)) install.packages("poibin")
if(!require(itertools)) install.packages("itertools")
if(!require(futile.logger)) install.packages("futile.logger")
if(!require(doSNOW)) install.packages("doSNOW")

library(doSNOW)
library(foreach)
library(ff)
library(futile.logger)

dir.create(file.path(getwd(), "tempfiles"))

print("Setting up parallel runtime...")
cat("\n")
cores = detectCores()
cl <- makeCluster(cores[1]-2)
registerDoSNOW(cl)
print("Parallel runtime setup complete.")


IPA <- function(input1 = NULL, dir1 = "current"){
  
  
          
  pcurver <- function(file1){
                
        require(stringr)
    require(poibin)
        
        options(scipen=999)
    
                    
                
    getncp.f =function(df1,df2, power)   {
      error = function(ncp_est, power, x, df1,df2) pf(x, df1 = df1, df2=df2, ncp = ncp_est) - (1-power)
      xc=qf(p=.95, df1=df1,df2=df2)
      return(uniroot(error, c(0, 1000), x = xc, df1 = df1,df2=df2, power=power)$root)  }
    
    
        getncp.c =function(df, power)   {
      xc=qchisq(p=.95, df=df)
      error = function(ncp_est, power, x, df)      pchisq(x, df = df, ncp = ncp_est) - (1-power)
      return(uniroot(error, c(0, 1000), x = xc, df = df, power=power)$root)   }
    
        getncp=function(family,df1,df2,power) {
      if (family=="f") ncp=getncp.f(df1=df1,df2=df2,power=power)
      if (family=="c") ncp=getncp.c(df=df1,power=power)
      return(ncp)  }
    
        percent <- function(x, digits = 0, format = "f", ...)   {
      paste(formatC(100 * x, format = format, digits = digits, ...), "%", sep = "")
    }
    
        pbound=function(p) pmin(pmax(p,2.2e-16),1-2.2e-16)
    
            prop33=function(pc){
            
                              
            prop=ifelse(family=="f" & p<.05,1-pf(qf(1-pc,df1=df1, df2=df2),df1=df1, df2=df2, ncp=ncp33),NA)
            prop=ifelse(family=="c" & p<.05,1-pchisq(qchisq(1-pc,df=df1),  df=df1, ncp=ncp33),prop)
            prop
    }
    
        stouffer=function(pp) sum(qnorm(pp),na.rm=TRUE)/sqrt(sum(!is.na(pp)))
    
                
        
        raw = file1          
    raw  = tolower(raw)                                       ktot = length(raw)                                        
        
    k=seq(from=1,to=length(raw))
    
            
        
    par3=str_locate(raw,"\\(")[,1]             stat=substring(raw,par3-1,par3-1)              stat[stat==2] <- "c"                  test=ifelse(stat=="r","t",stat)      
    
        
        
    family=test
    family=ifelse(test=="t","f",family)
    family=ifelse(test=="z","c",family)
    
                    
        par1 =str_locate(raw,"\\(")[,1]             par2 =str_locate(raw,"\\)")[,1]             comma=str_locate(raw,",")[,1]               eq   =str_locate(raw,"=")[,1]               
        df=as.numeric(ifelse(test=="t",substring(raw,par1+1,par2 -1),NA))                 
            df1=as.numeric(ifelse(test=="f",substring(raw,par1+1,comma-1),NA))                df1=as.numeric(ifelse(test=="z",1,df1))                                           df1=as.numeric(ifelse(test=="t",1,df1))                                           df1=as.numeric(ifelse(test=="c",substring(raw,par1+1,par2 -1),df1))               
        df2=as.numeric(ifelse(test=="f",substring(raw,comma+1,par2-1),NA))                df2=as.numeric(ifelse(test=="t",df,df2))                                          
        equal=abs(as.numeric(substring(raw,eq+1)))      
        value=ifelse((stat=="f" | stat=="c"),equal,NA)                          value=ifelse(stat=="r", (equal/(sqrt((1-equal**2)/df2)))**2,value)      value=ifelse(stat=="t", equal**2 ,value)                                value=ifelse(stat=="z", equal**2 ,value)
    
        p=ifelse(family=="f",1-pf(value,df1=df1,df2=df2),NA)
    p=ifelse(family=="c",1-pchisq(value,df=df1),p)
    p=pbound(p)      
            ksig= sum(p<.05,na.rm=TRUE)         khalf=sum(p<.025,na.rm=TRUE)        
                
        ppr=as.numeric(ifelse(p<.05,20*p,NA))                ppr=pbound(ppr)                                      
        ppr.half=as.numeric(ifelse(p<.025,40*p,NA))        ppr.half=pbound(ppr.half)
    
                ncp33=mapply(getncp,df1=df1,df2=df2,power=1/3,family=family)      
            pp33=ifelse(family=="f" & p<.05,3*(pf(value, df1=df1, df2=df2, ncp=ncp33)-2/3),NA)
    pp33=ifelse(family=="c" & p<.05,3*(pchisq(value, df=df1, ncp=ncp33)-2/3),pp33)
    pp33=pbound(pp33)
    
            prop25=3*prop33(.025)
    prop25.sig=prop25[p<.05]
    
        pp33.half=ifelse(family=="f" & p<.025, (1/prop25)*(    pf(value,df1=df1,df2=df2,ncp=ncp33)-(1-prop25)),NA)
    pp33.half=ifelse(family=="c" & p<.025, (1/prop25)*(pchisq(value,df=df1,         ncp=ncp33)-(1-prop25)),pp33.half)
    pp33.half=pbound(pp33.half)
    
                
        Zppr =     stouffer(ppr)                Zpp33=     stouffer(pp33)               Zppr.half= stouffer(ppr.half)           Zpp33.half=stouffer(pp33.half)          
        p.Zppr =pnorm(Zppr)	
    p.Zpp33=pnorm(Zpp33)
    p.Zppr.half =pnorm(Zppr.half)
    p.Zpp33.half=pnorm(Zpp33.half)
    
        main.results=as.numeric(c(ktot, ksig, khalf, Zppr, p.Zppr, Zpp33, p.Zpp33, Zppr.half, p.Zppr.half, Zpp33.half, p.Zpp33.half))
    names(main.results)=c("Total", "Significant", "Half", "Zppr", "p.Zppr", "Zpp33", "p.Zpp33", "Zppr.half", "p.Zppr.half", "Zpp33.half", "p.Zpp33.half")
    out1 <- main.results
    
            prop25.obs=sum(p<.025)/sum(p<.05)
        binom.r=1-pbinom(q=prop25.obs*ksig- 1, p=.5, size=ksig)             binom.33=ppoibin(kk=prop25.obs*ksig,pp=prop25[p<.05])
    
                                    
        binomial=c(mean(prop25.sig), prop25.obs, binom.r, binom.33)
    names(binomial)=c("prop25.sig", "prop25.obs", "binom.r", "binom.33")
    out1 <- c(out1, binomial)
    
                
                    powerfit=function(power_est)
    {
            ncp_est=mapply(getncp,df1=df1,df2=df2,power=power_est,family=family)
            pp_est=ifelse(family=="f" & p<.05,(pf(value,df1=df1,df2=df2,ncp=ncp_est)-(1-power_est))/power_est,NA)
      pp_est=ifelse(family=="c" & p<.05,(pchisq(value,df=df1,ncp=ncp_est)-(1-power_est))/power_est,pp_est)
      pp_est=pbound(pp_est)
            return(stouffer(pp_est))       }
    
                fit=c()                                              fit=abs(powerfit(.051))                          for (i in 6:99)   fit=c(fit,abs(powerfit(i/100)))     
        mini=match(min(fit,na.rm=TRUE),fit)
        hat=(mini+4)/100
    
            get.power_pct =function(pct)   {
                                          z=qnorm(pct)              error = function(power_est, z)  powerfit(power_est) - z
            return(uniroot(error, c(.0501, .99),z)$root)   }
    
            
        p.power.05=pnorm(powerfit(.051))     p.power.99=pnorm(powerfit(.99))      
            if (p.power.05<=.95) power.ci.lb=.05
        if (p.power.99>=.95) power.ci.lb=.99
        if (p.power.05>.95 && p.power.99<.95)  power.ci.lb=get.power_pct(.95)
    
            if (p.power.05<=.05) power.ci.ub=.05
        if (p.power.99>=.05) power.ci.ub=.99
        if (p.power.05>.05 && p.power.99<.05) power.ci.ub=get.power_pct(.05)
    
        power_results=c(power.ci.lb,hat,power.ci.ub)
    names(power_results)=c("power.ci.lb","hat","power.ci.ub")
    out1 <- c(out1, power_results)
    
            
                
        dropk=function(pp,k,droplow) {
                              
      pp=pp[!is.na(pp)]                                   n=length(pp)                                        pp=sort(pp)                                         if (k==0) ppk=pp                                          if (droplow==1 & k>0){
                ppk=(pp[(1+k):n])
        ppmin=min(pp[k],k/(n+1))           ppk=(ppk-ppmin)/(1-ppmin)                }
      
            if (droplow==0 & k>0)
      {
                ppk=pp[1:(n-k)]
        ppmax=max(pp[n-k+1],(n-k)/(n+1))          ppk=ppk/ppmax                            }
            ppk=pmax(ppk,.00001)                             ppk=pmin(ppk,.99999)                             Z=sum(qnorm(ppk))/sqrt(n-k)
      return(pnorm(Z))
    }
    
            droplow.r=droplow.33=drophigh.r=drophigh.33=c()
    
        for (i in 0:max(0,(round(ksig/2)-1)))
    {
                  droplow.r= c(droplow.r,   dropk(pp=ppr,k=i,droplow=1))
      drophigh.r=c(drophigh.r,  dropk(pp=ppr,k=i,droplow=0))
            droplow.33=c(droplow.33,  dropk(pp=pp33,k=i,droplow=1))
      drophigh.33=c(drophigh.33, dropk(pp=pp33,k=i,droplow=0))
    }
    
        if (khalf>0)
    {
      droplow.halfr=drophigh.halfr=c()
      for (i in 0:max(0,(round(khalf/2)-1)))
      {
                droplow.halfr= c(droplow.halfr,   dropk(pp=ppr.half,k=i,droplow=1))
        drophigh.halfr=c(drophigh.halfr,  dropk(pp=ppr.half,k=i,droplow=0))
      }     }
    out1
  }
  
    
    
    make.perm <- function(data){
    
    require(stringr)
    require(itertools)
    
    data[,2] <- as.character(data[, 2])
    things <- data[, 2]
    
    prefixes <- sub("\\(.*","",things)
    prefixes <- sub("\\=.*","",prefixes)
    prefixes <- sub("F.*","",prefixes)
    prefixes <- sub("z.*","",prefixes)
    prefixes <- sub("t.*","",prefixes)
    prefixes <- sub("chi2.*","",prefixes)
    prefixes <- sub("r.*","",prefixes)
    
    indexes <- data[, 1]
    
    expectedCombinedThingsNrows <- length(unique(data[prefixes != "",1]))
    
    combinedThings <- data.frame(
      "Article" = character(expectedCombinedThingsNrows),
      "Statistic" = character(expectedCombinedThingsNrows),
      stringsAsFactors = F
    )
    
    combinedThings_iter <- 1
    for(i in 1:length(unique(prefixes))) {
      prefix <- unique(prefixes)[i]
      if(prefix != "") {
        prefix_indx <- which(prefixes==prefix)
        prefixedThings <- data.frame(
          indexes = indexes[prefix_indx], 
          things = as.character(things[prefix_indx])
        )
        if(nrow(prefixedThings) > 1) {
          allThings <- prefixedThings[,1]
          uniqueThings <- unique(allThings)
          for(j in 1:length(uniqueThings)) {
                        insert <- prefixedThings[allThings==uniqueThings[j],][,2]
            if(length(insert) > 1) {
              for(k in 2:length(insert)) {
                                                                if(k == 2){
                  temp <- insert[1]
                  data <- data[as.character(data$Statistic) != insert[1],]
                }
                temp <- paste(temp, insert[k], sep="                temp <- cbind(as.character(uniqueThings[j]), temp) 
                                data <- data[as.character(data$Statistic)!=insert[k],]
              }
              
                            if(combinedThings_iter <= expectedCombinedThingsNrows){
                combinedThings[combinedThings_iter,] <- as.vector(temp)
                combinedThings_iter <- combinedThings_iter + 1
              } else {
                                stop("make.perm() error. There was an error while creating permutations")
              }
            }
          }
        }
      }
    }
    
    data <- rbind(data, combinedThings)
    
    indexes <- as.numeric(data[, 1])
    things <- data[, 2]
    
    splits <- split(things , f = indexes)
    
            
    orep <- prod(sapply(splits,length))
    
                
    require(ff)
    
                        
    if(orep*length(splits) > .Machine$integer.max) {
      print(paste0("Input file is very large. ", Sys.time()))
    }
    
    my.levels <- splits[[1]]
    for(kkk in 2:length(splits)){ my.levels <- c(my.levels,splits[[kkk]]) }
    
        write.csv(length(splits), file=paste0(getwd(), "/tempfiles/lengthsplits.csv"))
    
        write.csv(my.levels, file=paste0(getwd(), "/tempfiles/mylevels.csv"))
    
    rep.fac <- 1L
    orep <- orep
    
    cat("\n")
    print(paste0("Generating permutation values (step one). ", Sys.time()))
    cat("\n")
    
        for(j in seq_along(splits)) {
      x <- splits[[j]]
      nx <- length(x)
      orep <- orep/nx
      
                  permx <- as.ffdf(as.ff(factor(x[rep.int(rep.int(seq_len(nx), rep.int(rep.fac, nx)), orep)])))
      
                  cbind.ffdf2 <- function(d1, d2){
        D1names <- colnames(d1)
        D2names <- colnames(d2)
        mergeCall <- do.call("ffdf", c(physical(d1), physical(d2)))
        colnames(mergeCall) <- c(D1names, D2names)
        mergeCall
      }
      
            dimnames(permx)[[2]] <- j
      
      if(j==1){
        bigpermrow <- permx
      } else {
        bigpermrow <- cbind.ffdf2(bigpermrow, permx)
      }
      
      print(paste0("Permutation column ", j, " of ", length(splits),". ", Sys.time()))
      rep.fac  <- rep.fac * nx
    }
    
            assign("bigpermrow", bigpermrow, envir=.GlobalEnv, inherits=TRUE)
  }
  
    
    
    
  if(dir1 != "current") {setwd(dir1)} else {setwd(getwd())}
  
      
  inner_decision <- menu(c("R object", "tab-delimited file",
                           "csv (comma-separated) file"),
                         title = "Is the input an R object (data frame or matrix) or a file?")
  if(inner_decision == 1) {
    input1 <- get(readline(prompt = "object name: "))
  } else { 
    if(inner_decision == 2) {
      input1 <- read.table(file = file.choose(), sep = "\t", header = TRUE)
    } else {
      input1 <- read.table(file = file.choose(), sep = ",", header = TRUE)
    }
  }
  
    
  outer_decision <- menu(c("to R object and file", "to file"),
                         title = "Output to R object and file, or only to file?")
  outname <- readline(prompt = "output name (no suffix if file): ")
  
    
        
  make.perm(input1)
  
    permcols <- as.integer(read.csv(
    file=paste0(getwd(),"/tempfiles/lengthsplits.csv")))[2]
  
  print(paste0("Permutation table columns = ", ncol(bigpermrow), 
               "; Permutation table rows = ", nrow(bigpermrow), "."))
  
  waitlength <- nrow(bigpermrow)
  gc()
  
  
          
                
                  
    ncores <- length(cl)
  
      chunksize <- 192
  if(waitlength < chunksize){
    chunksize <- waitlength
  } 
  
  nchunks <- ceiling(waitlength/chunksize)
  
    if(nchunks >= length(cl)){
    ncores <- length(cl)
  } else if(nchunks < length(cl)){
    ncores <- nchunks
  }
  
  nloops <- ceiling(nchunks / ncores)
  
  cat("\n")
  print(paste0("Number of chunks = ", nchunks))
  print(paste0("Number of cores = ", ncores))
  print(paste0("Lines per chunk = ", chunksize))
  print(paste0("Number of loops = ", nloops))
  
  cols.bigpermrow <- paste0("x",rep(1:ncol(bigpermrow)))
  cols <- c("Total",	"Significant",	"Half",	"Zppr",	"p.Zppr",
            "Zpp33",	"p.Zpp33",	"Zppr.half",	"p.Zppr.half",	
            "Zpp33.half",	"p.Zpp33.half",	"prop25.sig",
            "prop25.obs",	"binom.r",	"binom.33",	"power.ci.lb",
            "hat",	"power.ci.ub", cols.bigpermrow)
  
  gc()
  
  cat("\n")
  print(paste0("Running pcurver (step two). ", Sys.time()))
  cat("\n")
  
      for(loop in 1:nloops){
    
        idx <- seq((loop-1)*chunksize*ncores+1, loop*chunksize*ncores, 1)
    if(loop == nloops){
      idx <- idx[idx <= waitlength]
    }
    temppermtable <- bigpermrow[idx,]
    
        if(nrow(temppermtable) < ncores){
      ncores = nrow(temppermtable)
    }
        outietemp <- foreach (core=1:ncores, .combine="rbind", .inorder=TRUE,
                          .packages="ff") %dopar% {
                            
                                                        waitlength2 <- nrow(temppermtable)
                            idx2 <- seq(core, waitlength2, ncores)
                            corewise_permtable <- temppermtable[idx2, ]
                            
                                                        for(i in 1:nrow(corewise_permtable)){
                              permrow <- unlist(corewise_permtable[i,])
                              
                                                            pcurver_param <- ""
                              
                              if(length(grep("                                perm_subset <- as.character(permrow[grep("                                                                splitTests <- as.data.frame(unlist(strsplit(perm_subset,split='                                F1 <- as.data.frame(permrow[!(permrow %in% perm_subset)])
                                colnames(F1) <- "Stats"
                                colnames(splitTests) <- "Stats"
                                F1 <- rbind(F1,splitTests)
                                pcurver_param <- as.character(F1[1:nrow(F1),])
                              } else {
                                pcurver_param <- as.character(permrow)
                              }
                              
                              if(i == 1){
                                pout <- as.data.frame(matrix(NA, nrow=nrow(corewise_permtable), 
                                                             ncol=length(cols), byrow=FALSE))
                              }
                              pout[i, ] <- as.character(c(pcurver(pcurver_param), as.character(permrow)))
                            }
                            return(pout)
                          }
    
        outietemp <- as.data.frame(outietemp)
    rownames(outietemp) <- as.character(idx)
    colnames(outietemp) <- cols
    
    if(loop==1){
      write.table(outietemp, file=paste0(getwd(), "/", outname, ".txt"),
                  sep="\t", append=FALSE, row.names=FALSE)
    } else if(loop>1){
      write.table(outietemp, file=paste0(getwd(), "/", outname, ".txt"),
                  sep="\t", append=TRUE, row.names=FALSE, col.names=FALSE)
    }
    
    print(paste0("pcurver ", round(idx[length(idx)]/waitlength*100, 4),
                 " percent finished: lines ", idx[length(idx)], " of ",
                 waitlength, " written. ", Sys.time()))
    
    rm(temppermtable)
    rm(outietemp)
    rm(idx)
  }
  
  gc()
  
  cat("\n")
  print(paste0("pcurver completed! ", Sys.time()))
  
    if(outer_decision == 1) {
    cat("\n")
    print(paste0("Loading output into an 'ff' package R object. ", Sys.time()))
    
    outie <- read.table.ffdf(file=paste0(getwd(), "/", outname, ".txt"),
                             next.rows=waitlength, header=TRUE)
    
    assign(outname, outie, envir=.GlobalEnv)
    rm(outie)
  }
  
    cat("\n")
  print("Removing temporary files.")
  unlink("tempfiles", recursive=TRUE)
  
  cat("\n")
  print(paste0("Process complete! ", Sys.time()))
  cat("\n")
  print(paste0("pcurver values are saved in file ", outname, ".txt"))
  if(outer_decision == 1) {
    print(paste0("pcurver values are also stored in R object ", outname))
  }
  
  invisible(stopCluster(cl))
  invisible(gc())
  
}



