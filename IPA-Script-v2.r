# A script that:
#   a) creates a table of all possible permutations of an input file
#   b) performs the 'pcurver' function on each row
#   c) creates a file containing permutation and pcurver output data 

# install and load packages
if(!require(doParallel)) install.packages("doParallel")
if(!require(foreach)) install.packages("foreach")
if(!require(ff)) install.packages("ff")
if(!require(stringr)) install.packages("stringr")
if(!require(poibin)) install.packages("poibin")
if(!require(itertools)) install.packages("itertools")
if(!require(futile.logger)) install.packages("futile.logger")
if(!require(doSNOW)) install.packages("doSNOW")

library(doSNOW)
library(foreach)
library(ff)
library(futile.logger)

# create a directory for temporary files
dir.create(file.path(getwd(), "tempfiles"))

# Set up the parallel runtime
print("Setting up parallel runtime...")
cat("\n")
cores = detectCores()
cl <- makeCluster(cores[1]-2)
registerDoSNOW(cl)
print("Parallel runtime setup complete.")


########################################################################################


#This is a wrapper for the pcurver function that handles input and output and analyzing all permutations of inputs.
IPA <- function(input1 = NULL, dir1 = "current"){
  
  
  #This is a function that does the permuting. It presumes that the input is a matrix or data frame with two columns. The first 
  #column must be study identifiers, be they numbers, letters, paper names, etc. The second column must take the
  #appropriate format of study statistic description as used for pcurve.
  ### ERL 180921 modified this to categorize groups ###
  
  pcurver <- function(file1){
    #Based on pcurve_app script by Uri Simonsohn (urisohn@gmail.com), 2017 11 30
    #This is a stripped-down output version designed to output for later processing. No figures are generated.
    #All results of the calculations are output as a single vector.
    
    #0.0 Load required libraries
    require(stringr)
    require(poibin)
    #0.1 Set up parameters
    
    #Disable scientific notation
    options(scipen=999)
    
    ##############################################
    #(0) CREATE A FEW FUNCTIONS
    ##############################################
    #Function 1 - functions that find non-centrality parameter for f,chi distributions that gives some level of power
    
    #F-test
    #Note: starting with app 4.0, t() are converted to F() and Z to chisq() to avoid unnecessary repetition of code
    #So we only need code to find ncp for F() and chisq()
    
    getncp.f =function(df1,df2, power)   {
      error = function(ncp_est, power, x, df1,df2) pf(x, df1 = df1, df2=df2, ncp = ncp_est) - (1-power)
      xc=qf(p=.95, df1=df1,df2=df2)
      return(uniroot(error, c(0, 1000), x = xc, df1 = df1,df2=df2, power=power)$root)  }
    
    
    #chisq-test
    getncp.c =function(df, power)   {
      xc=qchisq(p=.95, df=df)
      error = function(ncp_est, power, x, df)      pchisq(x, df = df, ncp = ncp_est) - (1-power)
      return(uniroot(error, c(0, 1000), x = xc, df = df, power=power)$root)   }
    
    #Combine both in single function
    getncp=function(family,df1,df2,power) {
      if (family=="f") ncp=getncp.f(df1=df1,df2=df2,power=power)
      if (family=="c") ncp=getncp.c(df=df1,power=power)
      return(ncp)  }
    
    #Function 2 - percent() : makes a number look like a percentage
    percent <- function(x, digits = 0, format = "f", ...)   {
      paste(formatC(100 * x, format = format, digits = digits, ...), "%", sep = "")
    }

    #Function 3 - pbound: bound p-values and pp-values by precision of measurement to avoid errors
    pbound=function(p) pmin(pmax(p,2.2e-16),1-2.2e-16)
    
    #Function 4 - prop33(pc) - Computes % of p-values that are expected to be smaller than pc,
    #for the tests submitted to p-curve, if power is 33%
    prop33=function(pc){
      #pc: critical  p-value
      
      #Overview:
      #Creates a vector of the same length as the number of tests submitted to p-curve, significant and not,
      #    and computes the proportion of p-values expected to be smaller than {pc} given the d.f.
      #    and outputs the entire vector, with NA values where needed
      
      #F-tests (& thus  t-tests)
      prop=ifelse(family=="f" & p<.05,1-pf(qf(1-pc,df1=df1, df2=df2),df1=df1, df2=df2, ncp=ncp33),NA)
      #Chi2 (& thus Normal)
      prop=ifelse(family=="c" & p<.05,1-pchisq(qchisq(1-pc,df=df1),  df=df1, ncp=ncp33),prop)
      #output it
      prop
    }
    
    #Function 5 Stouffer test for a vector of pp-values
    stouffer=function(pp) sum(qnorm(pp),na.rm=TRUE)/sqrt(sum(!is.na(pp)))
    
    ###############################################################################
    #(1) PROCESS USER INPUT AND CONVERT IT INTO USABLE R DATA
    ###############################################################################
    
    #(1.1) Load data
    
    #From file;
    raw = file1      #read file on server    
    
    raw  = tolower(raw)                                   #lower case
    ktot = length(raw)                                    #count studies
    
    #Create vector that numbers studies 1 to N,includes n.s. studies
    
    k=seq(from=1,to=length(raw))
    
    #1.2 Parse the entered text into usable statistical results
    #1.3 Create test type indicator
    
    ###ERL This is where the magic needs to happen ##########################
    
    par3=str_locate(raw,"\\(")[,1]         #(  First  parenthesis
    stat=substring(raw,par3-1,par3-1)          #stat:   t,f,z,c,r
    stat[stat==2] <- "c"              #ERL FIX 181003
    test=ifelse(stat=="r","t",stat)  #test:   t,f,z,c      (r-->t)
    
    
    #########################################################################
    
    #1.4 Create family to turn t-->F and z-->chi2
    
    family=test
    family=ifelse(test=="t","f",family)
    family=ifelse(test=="z","c",family)
    
    #Note on terminology:
    #Stat:   t,f,c,z,r  is what the user entered, t,f,c,z,r
    #test:   t,f,c,z    is the test statistic, same as stat but with r-->t
    #family: f,c        converting t-->f and z-->c
    
    #1.5 Find comma,parentheses,equal sign
    par1 =str_locate(raw,"\\(")[,1]         #(  First  parenthesis
    par2 =str_locate(raw,"\\)")[,1]         #)  Second parenthesis
    comma=str_locate(raw,",")[,1]           #,  comma
    eq   =str_locate(raw,"=")[,1]           #=  equal
    
    #1.6 DF for t-tests
    df=as.numeric(ifelse(test=="t",substring(raw,par1+1,par2 -1),NA))             #t(df) later assigned to df2 in  F test with df1=1
    
    #1.7 DF1 for all tests
    #   recall, normal=sqrt(chi(1)) so df1=1 for Normal, same f(1,df)<-t(df)
    df1=as.numeric(ifelse(test=="f",substring(raw,par1+1,comma-1),NA))            #If F test, take value after comma, NA otherwise
    df1=as.numeric(ifelse(test=="z",1,df1))                                       #If Z replace missing value with a 1
    df1=as.numeric(ifelse(test=="t",1,df1))                                       #If t, replace missing value with a 1
    df1=as.numeric(ifelse(test=="c",substring(raw,par1+1,par2 -1),df1))           #If c, replace missing value with value in ()
    
    #1.8 DF2 for F(df1,df2) tests
    df2=as.numeric(ifelse(test=="f",substring(raw,comma+1,par2-1),NA))            #F-test
    df2=as.numeric(ifelse(test=="t",df,df2))                                      #t(df) into the df2 F test
    
    #1.9 Take value after equal sign, the value of the test-statistic, and put it in vector "equal"
    equal=abs(as.numeric(substring(raw,eq+1)))  #if not a r(), take the value after the ="
    
    #1.10  Go from "equal" (the value after = sign) to F or Chi2 value,
    value=ifelse((stat=="f" | stat=="c"),equal,NA)                      #For F and Chi2 test, equal=value
    value=ifelse(stat=="r", (equal/(sqrt((1-equal**2)/df2)))**2,value)  #For correlation, first turn value (r) to t, then square t. (using t=r/sqrt(1-r**2)/DF)
    value=ifelse(stat=="t", equal**2 ,value)                            #For t and Z, square it since t(df)**2=f(1,df) and z**2=chi(1)
    value=ifelse(stat=="z", equal**2 ,value)
    
    #1.11 Compute p-values
    p=ifelse(family=="f",1-pf(value,df1=df1,df2=df2),NA)
    p=ifelse(family=="c",1-pchisq(value,df=df1),p)
    p=pbound(p)  #Bound it to level of precision, see function 3 above
    
    #1.12 Count  studies
    #ktot is all studies
    ksig= sum(p<.05,na.rm=TRUE)     #significant studies
    khalf=sum(p<.025,na.rm=TRUE)    #half p-curve studies
    
    ###############################################################################
    #(2) COMPUTE PP-VALUES
    ##############################################################################
    
    #2.1 Right Skew, Full p-curve
    ppr=as.numeric(ifelse(p<.05,20*p,NA))            #If p<.05, ppr is 1/alpha*p-value, so 20*pvalue, otherwise missing.
    ppr=pbound(ppr)                                  #apply pbound function to avoid 0
    
    #2.2 Right Skew, half p-curve
    ppr.half=as.numeric(ifelse(p<.025,40*p,NA))    #If p<.05, ppr is 40*pvalue, otherwise missing.
    ppr.half=pbound(ppr.half)
    
    #2.3 Power of 33%
    #2.3.1 NCP for  f,c distributions
    # NCP33 (noncentrality parameter giving each test in p-curve 33% power given the d.f. of the test)
    ncp33=mapply(getncp,df1=df1,df2=df2,power=1/3,family=family)  #See function 1 above
    
    #2.3.2 Full-p-curve
    #Using the ncp33 compute pp33
    pp33=ifelse(family=="f" & p<.05,3*(pf(value, df1=df1, df2=df2, ncp=ncp33)-2/3),NA)
    pp33=ifelse(family=="c" & p<.05,3*(pchisq(value, df=df1, ncp=ncp33)-2/3),pp33)
    pp33=pbound(pp33)
    
    #2.3.3 HALF-p-curve
    #Share of p-values expected to be p<.025 if 33% power (using Function 4 from above, prop33() )
    prop25=3*prop33(.025)
    prop25.sig=prop25[p<.05]
    
    #2.3.4 Compute pp-values for the half
    pp33.half=ifelse(family=="f" & p<.025, (1/prop25)*(    pf(value,df1=df1,df2=df2,ncp=ncp33)-(1-prop25)),NA)
    pp33.half=ifelse(family=="c" & p<.025, (1/prop25)*(pchisq(value,df=df1,         ncp=ncp33)-(1-prop25)),pp33.half)
    pp33.half=pbound(pp33.half)
    
    ###############################################################################
    #(3) INFERENCE - STOUFFER & BINOMIAL
    ##############################################################################
    
    #3.1 Convert pp-values to Z scores, using Stouffer function above
    Zppr =     stouffer(ppr)            #right skew  - this is a Z value from Stouffer's test
    Zpp33=     stouffer(pp33)           #33% - idem
    Zppr.half= stouffer(ppr.half)       #right skew, half p-curve - idem
    Zpp33.half=stouffer(pp33.half)      #33% skew, half p-curve - idem
    
    #3.2 Overall p-values from Stouffer test
    p.Zppr =pnorm(Zppr)	
    p.Zpp33=pnorm(Zpp33)
    p.Zppr.half =pnorm(Zppr.half)
    p.Zpp33.half=pnorm(Zpp33.half)
    
    #3.3 Save results to file
    main.results=as.numeric(c(ktot, ksig, khalf, Zppr, p.Zppr, Zpp33, p.Zpp33, Zppr.half, p.Zppr.half, Zpp33.half, p.Zpp33.half))
    names(main.results)=c("Total", "Significant", "Half", "Zppr", "p.Zppr", "Zpp33", "p.Zpp33", "Zppr.half", "p.Zppr.half", "Zpp33.half", "p.Zpp33.half")
    out1 <- main.results
    
    #3.4 BINOMIAL
    #Observed share of p<.025
    prop25.obs=sum(p<.025)/sum(p<.05)
    #3.4.1 Flat null
    binom.r=1-pbinom(q=prop25.obs*ksig- 1, p=.5, size=ksig)     #The binomial in R computes the probability of x<=xo. We want prob(x>=x0) so we subtract one from x, and 1-prob()
    #3.4.2 Power of 33% null
    binom.33=ppoibin(kk=prop25.obs*ksig,pp=prop25[p<.05])
    
    #syntax for ppoibin():
    #   kk: is the proportion of succeses, a scalar, in this case, the share of p<.025
    #   pp: is the probabilty of success for each attempt, the number of attempts is determined
    #    by the length of the vector. For example ppoibin(kk=0,pp=c(.5,.5,.5)) is .125,
    #    if there are three attempts, each with probability .5, the odds of getting 0 succeses is .125
    #     ppoibin(kk=1,pp=c(1,.75)), in turn computes the probability of getting 1 success
    #     when one has a 100% of success, and the other 75%, and the solution is .25, since
    #     the first one succeeds for sure and the second would need to fail, with 25% chance.
    
    #3.4.3  Save binomial results
    binomial=c(mean(prop25.sig), prop25.obs, binom.r, binom.33)
    names(binomial)=c("prop25.sig", "prop25.obs", "binom.r", "binom.33")
    out1 <- c(out1, binomial)
    
    ################################################
    #(4) POWER ESTIMATE
    ################################################
    
    #4.1 Function powerfit(power_est) - Returns the Stouffer Z of observing at least as right skewed a p-curve if  power=power_est
    #if Z=0, power_est is the best fit (p=50%).
    #if Z<0 the best fit is <power_est,
    #if Z>0 the best fit is >power_est
    powerfit=function(power_est)
    {
      #4.1.1 Get the implied non-centrality parameters (ncps) that give power_est to each test submitted to p-curve
      ncp_est=mapply(getncp,df1=df1,df2=df2,power=power_est,family=family)
      #4.1.2 Compute implied pp-values from those ncps_est,
      pp_est=ifelse(family=="f" & p<.05,(pf(value,df1=df1,df2=df2,ncp=ncp_est)-(1-power_est))/power_est,NA)
      pp_est=ifelse(family=="c" & p<.05,(pchisq(value,df=df1,ncp=ncp_est)-(1-power_est))/power_est,pp_est)
      pp_est=pbound(pp_est)
      #4.1.3 Aggregate pp-values for null that power=power_est via Stouffer
      return(stouffer(pp_est))   #This is a z score, so powerfit is expressed as the resulting Z score.
    }
    
    #4.2 COMPUTE FIT FOR EACH POWER for 5.1%, AND THEN 6-99%, AND PLOT IT. With power=5% boundary condition lead to errors
    #This becomes the diagnostic plot and gives us the best estimate, within 1%, of power.
    # Fit will be evaluated at every possible value of power between 5.1% and 99% in steps of 1%, stored in fit()
    fit=c()                                          #Create empty vector
    fit=abs(powerfit(.051))                      #Start it eavaluting fit of 5.1% power
    for (i in 6:99)   fit=c(fit,abs(powerfit(i/100))) #Now do 6% to 99%
    
    # Find the minimum which ith power level considered leads to best estimate
    mini=match(min(fit,na.rm=TRUE),fit)
    #convert that into the power level, the ith value considered is (5+ith)/100
    hat=(mini+4)/100
    
    #4.3 Confidence interval for power estimate
    #4.3.1 Function get.power_pct(pct)
    get.power_pct =function(pct)   {
      #Function that finds power that gives p-value=pct for the Stouffer test
      #for example, get.power_pct(.5) returns the level of power that leads to p=.5  for the stouffer test.
      #half the time we would see p-curves more right skewed than the one we see, and half the time
      #less right-skewed, if the true power were that get.power_pct(.5). So it is the median estimate of power
      #similarliy, get.power_pct(.1) gives the 10th percentile estimate of power...
      #Obtain the normalized equivalent of pct, e.g., for 5% it is -1.64, for 95% it is 1.64
      z=qnorm(pct)  #convert to z because powerfit() outputs a z-score.
      #Quantify gap between computed p-value and desired pct
      error = function(power_est, z)  powerfit(power_est) - z
      #Find the value of power that makes that gap zero, (root)
      return(uniroot(error, c(.0501, .99),z)$root)   }
    
    #4.3.2 Boundary conditions (when the end of the ci=5% or 99% we cannot use root to find it,
    #use boundary value instead)
    
    #Boundary conditions
    p.power.05=pnorm(powerfit(.051)) #Proability p-curve would be at least at right-skewed if power=.051
    p.power.99=pnorm(powerfit(.99))  #Proability p-curve would be at least at right-skewed if power=.99
    
    #4.3.3 Find lower end of ci
    #Low boundary condition? If cannot reject 5% power, don't look for lower levels, use 5% as the end
    if (p.power.05<=.95) power.ci.lb=.05
    #High boundary condition? If we reject 99%, from below dont look for higher power, use 99% as the low end
    if (p.power.99>=.95) power.ci.lb=.99
    #If low bound is higher than 5.1% power and lower than 99% power, estimate it, find interior solution
    if (p.power.05>.95 && p.power.99<.95)  power.ci.lb=get.power_pct(.95)
    
    #4.3.4 Higher end of CI
    #If we reject 5% power from below, 5% is above the confidence interval, use 5% as the upper end of the confidence interval
    if (p.power.05<=.05) power.ci.ub=.05
    #If we do not reject that 99% power, don't look higher, use 99% as the higher end
    if (p.power.99>=.05) power.ci.ub=.99
    #If the the upper bound is between 5% and 99%, find it
    if (p.power.05>.05 && p.power.99<.05) power.ci.ub=get.power_pct(.05)
    
    #4.4 Save power fit estimate and ci
    power_results=c(power.ci.lb,hat,power.ci.ub)
    names(power_results)=c("power.ci.lb","hat","power.ci.ub")
    out1 <- c(out1, power_results)
    
    #Hat is used as the estimate of power, with powerfit(.5) we could get a more precise best fitting
    #level of power than the minimum in the figure above between .051 and .99, hat, but more precision than 1% in power is not informative.
    
    ################################################
    # 5 CUMULATIVE P-CURVES
    ################################################
    
    #5.1 FUNCTION THAT RECOMPUTES OVERALL STOUFFER TEST WITHOUT (K) MOST EXTREME VALUES, ADJUSTING THE UNIFORM TO ONLY INCLUDE RANGE THAT REMAINS
    dropk=function(pp,k,droplow) {
      #Syntax:
      #pp: set of pp-values to analyze sensitivity to most extremes
      #k:  # of most extreme values to exclude
      #dropsmall: 1 to drop smallest, 0 to drop largest
      
      pp=pp[!is.na(pp)]                             #Drop missing values
      n=length(pp)                                  #See how many studies are left
      pp=sort(pp)                                   #Sort the pp-value from small to large
      if (k==0) ppk=pp                              #If k=0 do nothing for nothing is being dropped
      #If we are dropping low values
      if (droplow==1 & k>0){
        #Eliminate lowest k from the vector of pp-values
        ppk=(pp[(1+k):n])
        ppmin=min(pp[k],k/(n+1))   #Boundary used to define possible range of values after exclusion
        ppk=(ppk-ppmin)/(1-ppmin)  #Take the k+1 smallest pp-value up to the highest, subtract from each the boundary value, divide by the range, ~U(0,1) under the null
        #This is explained in Supplement 1 of Simonsohn, Simmons Nelson, JEPG 2016 "Better p-curves" paper. See https://osf.io/mbw5g/
      }
      
      #If we are dropping high values
      if (droplow==0 & k>0)
      {
        #Eliminate lowest k from the vector of pp-values
        ppk=pp[1:(n-k)]
        ppmax=max(pp[n-k+1],(n-k)/(n+1))  #Find new boundary of range
        ppk=ppk/ppmax                      #Redefine range to make U(0,1)
      }
      #In case of a tie with two identical values we would have the ppk be 0 or 1, let's replace that with almost 0 and almost 1
      ppk=pmax(ppk,.00001)                       #Adds small constant to the smallest redefined p-value, avoids problem if dropped p-value is "equal" to next highest, then that pp-value becomes 0
      ppk=pmin(ppk,.99999)                       #Subtract small constant to the largest  redefined pp-value, same reason
      Z=sum(qnorm(ppk))/sqrt(n-k)
      return(pnorm(Z))
    }
    
    #5.2 Apply function, in loop with increasing number of exclusions, to full p-curve
    #Empty vectors for results
    droplow.r=droplow.33=drophigh.r=drophigh.33=c()
    
    #Loop over full p-curves
    for (i in 0:max(0,(round(ksig/2)-1)))
    {
      #Drop the lowest k studies in terms of respective overall test
      #Right skew
      droplow.r= c(droplow.r,   dropk(pp=ppr,k=i,droplow=1))
      drophigh.r=c(drophigh.r,  dropk(pp=ppr,k=i,droplow=0))
      #Power of 33%
      droplow.33=c(droplow.33,  dropk(pp=pp33,k=i,droplow=1))
      drophigh.33=c(drophigh.33, dropk(pp=pp33,k=i,droplow=0))
    }
    
    #Half p-curves
    if (khalf>0)
    {
      droplow.halfr=drophigh.halfr=c()
      for (i in 0:max(0,(round(khalf/2)-1)))
      {
        #Drop the lowest k studies in terms of respective overall test
        droplow.halfr= c(droplow.halfr,   dropk(pp=ppr.half,k=i,droplow=1))
        drophigh.halfr=c(drophigh.halfr,  dropk(pp=ppr.half,k=i,droplow=0))
      } #End loop
    }
    out1
  }
  
  # End of the pcurver function
  
  ########################################################################################
  
  # make permutation files
  make.perm <- function(data){
    
    require(stringr)
    require(itertools)
    
    data[,2] <- as.character(data[, 2])
    things <- data[, 2]
    

    prefixes <- sub("\\(.*","",things)
    prefixes <- sub("\\=.*","",prefixes)
    prefixes <- sub("F.*","",prefixes)
    prefixes <- sub("z.*","",prefixes)
    prefixes <- sub("t.*","",prefixes)
    prefixes <- sub("chi2.*","",prefixes)
    prefixes <- sub("r.*","",prefixes)
    
    indexes <- data[, 1]
    
    expectedCombinedThingsNrows <- length(unique(data[prefixes != "",1]))
    
    combinedThings <- data.frame(
      "Article" = character(expectedCombinedThingsNrows),
      "Statistic" = character(expectedCombinedThingsNrows),
      stringsAsFactors = F
    )
    
    combinedThings_iter <- 1
    for(i in 1:length(unique(prefixes))) {
      prefix <- unique(prefixes)[i]
      if(prefix != "") {
        prefix_indx <- which(prefixes==prefix)
        prefixedThings <- data.frame(
          indexes = indexes[prefix_indx], 
          things = as.character(things[prefix_indx])
        )
        if(nrow(prefixedThings) > 1) {
          allThings <- prefixedThings[,1]
          uniqueThings <- unique(allThings)
          for(j in 1:length(uniqueThings)) {
            ## cat("> j:", j, "\n")
            insert <- prefixedThings[allThings==uniqueThings[j],][,2]
            if(length(insert) > 1) {
              for(k in 2:length(insert)) {
                # When for loop for this k's is run for the first time,
                # initialize `temp` variable and let that variable carry over
                # to the next k+1 iteration
                if(k == 2){
                  temp <- insert[1]
                  data <- data[as.character(data$Statistic) != insert[1],]
                }
                temp <- paste(temp, insert[k], sep="#")
                temp <- cbind(as.character(uniqueThings[j]), temp) 
                # Remove the relevant rows from data
                data <- data[as.character(data$Statistic)!=insert[k],]
              }
              
              # Update combinedThings
              if(combinedThings_iter <= expectedCombinedThingsNrows){
                combinedThings[combinedThings_iter,] <- as.vector(temp)
                combinedThings_iter <- combinedThings_iter + 1
              } else {
                # Too many combined things than the expected count
                stop("make.perm() error. There was an error while creating permutations")
              }
            }
          }
        }
      }
    }
    
    data <- rbind(data, combinedThings)
    
    indexes <- as.numeric(data[, 1])
    things <- data[, 2]
    
    splits <- split(things , f = indexes)
    
    # Get permutations by first initializing
    # update values (similar logic found in expand.grid)
    
    orep <- prod(sapply(splits,length))
    
    # Here, we create the permutation matrix
    # This uses hard disk drive space or the 'ff'
    # package to store output files
    
    require(ff)
    
    # Very large permutation table outputs exceed '.Machine$integer.max'
    # values set by R (related to floating point integer size)
    # and the column-by-column method of creating the permutation table
    # reflects this. 
    # Give a notice for 'large' input sets
    
    if(orep*length(splits) > .Machine$integer.max) {
      print(paste0("Input file is very large. ", Sys.time()))
    }
    
    my.levels <- splits[[1]]
    for(kkk in 2:length(splits)){ my.levels <- c(my.levels,splits[[kkk]]) }
    
    # save the splits length
    write.csv(length(splits), file=paste0(getwd(), "/tempfiles/lengthsplits.csv"))
    
    # save my.levels
    write.csv(my.levels, file=paste0(getwd(), "/tempfiles/mylevels.csv"))
    
    rep.fac <- 1L
    orep <- orep
    
    cat("\n")
    print(paste0("Generating permutation values (step one). ", Sys.time()))
    cat("\n")
    
    # generate all permutations
    for(j in seq_along(splits)) {
      x <- splits[[j]]
      nx <- length(x)
      orep <- orep/nx
      
      # Cengiz: Note we are working with factors now, because ff works with factors not characters
      # jr: generate permutations - column-wise
      permx <- as.ffdf(as.ff(factor(x[rep.int(rep.int(seq_len(nx), rep.int(rep.fac, nx)), orep)])))
      
      # cbind-like function for use with package 'ff' without loading into memory
      # from user "Audrey" at stack exchange: https://stackoverflow.com/questions/20602751/how-to-column-bind-two-ffdf)
      cbind.ffdf2 <- function(d1, d2){
        D1names <- colnames(d1)
        D2names <- colnames(d2)
        mergeCall <- do.call("ffdf", c(physical(d1), physical(d2)))
        colnames(mergeCall) <- c(D1names, D2names)
        mergeCall
      }
      
      # change colnames (cbind.ffdf2 function requires unique colnames)
      dimnames(permx)[[2]] <- j
      
      if(j==1){
        bigpermrow <- permx
      } else {
        bigpermrow <- cbind.ffdf2(bigpermrow, permx)
      }
      
      print(paste0("Permutation column ", j, " of ", length(splits),". ", Sys.time()))
      rep.fac  <- rep.fac * nx
    }
    
    # Assign the permutation table to the global environment
    # for use by the pcurver
    assign("bigpermrow", bigpermrow, envir=.GlobalEnv, inherits=TRUE)
  }
  
  # End of make.perm
  
  #######################################################################################################  
  
  # Set the working directory, if supplied, or use the current working directory.
  
  if(dir1 != "current") {setwd(dir1)} else {setwd(getwd())}
  
  # Ask the user for input type and location.
  # If input is a file, it must be in the working directory.
  
  inner_decision <- menu(c("R object", "tab-delimited file",
                           "csv (comma-separated) file"),
                         title = "Is the input an R object (data frame or matrix) or a file?")
  if(inner_decision == 1) {
    input1 <- get(readline(prompt = "object name: "))
  } else { 
    if(inner_decision == 2) {
      input1 <- read.table(file = file.choose(), sep = "\t", header = TRUE)
    } else {
      input1 <- read.table(file = file.choose(), sep = ",", header = TRUE)
    }
  }
  
  # Ask user for output format: R object + file or file only
  
  outer_decision <- menu(c("to R object and file", "to file"),
                         title = "Output to R object and file, or only to file?")
  outname <- readline(prompt = "output name (no suffix if file): ")
  
  #################################################################################
  
  # The meat of the function. Permute the input by study
  # (one statistic per study), apply the pcurver script
  # to each permutation, and return the results.
  
  make.perm(input1)
  
  # load data saved in previous step
  permcols <- as.integer(read.csv(
    file=paste0(getwd(),"/tempfiles/lengthsplits.csv")))[2]
  
  print(paste0("Permutation table columns = ", ncol(bigpermrow), 
               "; Permutation table rows = ", nrow(bigpermrow), "."))
  
  waitlength <- nrow(bigpermrow)
  gc()
  
  
  # The pcurver function works on every row of the (sometimes massive)
  # permutation table. A combination of loops and parallel processing
  # are used to minimize RAM usage ('ff' is used for large objects)
  # and to write output to a single output file in a step-wise manner.
  
  # More specifically, the permutation table is chunked into
  # RAM-appropriate sizes (chunksize <- 192, adjustable) and a for loop 
  # based on the number of resulting chunks is created. Within this loop, chunks
  # are parceled out to foreach %dopar% (parallel processing) threads,
  # within which a loop runs the pcurver on each chunk line.
  # If chunksize is not a numeric factor of the total number of permutations,
  # the last loop will be shorter (all remaining lines).
  
  # Graphically speaking, in code:
  # for (chunk in 1:number_of_chunks/number of cores) {
  #   foreach(core = number_of_available_cores){
  #     for (line in 1:number_of_permtable_lines){
  #       pcurver(permtable[line,])
  #     }
  #   }
  # }
  
  # set the number of cores to be used based on permutation table size.
  ncores <- length(cl)
  
  # set a value for the number of permutation table lines to be
  # to be processed by each core
  chunksize <- 192
  if(waitlength < chunksize){
    chunksize <- waitlength
  } 
  
  nchunks <- ceiling(waitlength/chunksize)
  
  # set the number of cores to be used
  if(nchunks >= length(cl)){
    ncores <- length(cl)
  } else if(nchunks < length(cl)){
    ncores <- nchunks
  }
  
  nloops <- ceiling(nchunks / ncores)
  
  cat("\n")
  print(paste0("Number of chunks = ", nchunks))
  print(paste0("Number of cores = ", ncores))
  print(paste0("Lines per chunk = ", chunksize))
  print(paste0("Number of loops = ", nloops))
  
  cols.bigpermrow <- paste0("x",rep(1:ncol(bigpermrow)))
  cols <- c("Total",	"Significant",	"Half",	"Zppr",	"p.Zppr",
            "Zpp33",	"p.Zpp33",	"Zppr.half",	"p.Zppr.half",	
            "Zpp33.half",	"p.Zpp33.half",	"prop25.sig",
            "prop25.obs",	"binom.r",	"binom.33",	"power.ci.lb",
            "hat",	"power.ci.ub", cols.bigpermrow)
  
  gc()
  
  cat("\n")
  print(paste0("Running pcurver (step two). ", Sys.time()))
  cat("\n")
  
  # run the pcurver in a loop, split sub-structurally into
  # parallel processes
  for(loop in 1:nloops){
    
    # create a loop-wise permutation table subset
    idx <- seq((loop-1)*chunksize*ncores+1, loop*chunksize*ncores, 1)
    if(loop == nloops){
      idx <- idx[idx <= waitlength]
    }
    temppermtable <- bigpermrow[idx,]
    
    # make sure no empty rows are passed to the pcurver
    if(nrow(temppermtable) < ncores){
      ncores = nrow(temppermtable)
    }
    # run the pcurver, parallelized
    outietemp <- foreach (core=1:ncores, .combine="rbind", .inorder=TRUE,
                          .packages="ff") %dopar% {
                            
                            # create a core-wise permutation table subset
                            waitlength2 <- nrow(temppermtable)
                            idx2 <- seq(core, waitlength2, ncores)
                            corewise_permtable <- temppermtable[idx2, ]
                            
                            # run the pcurver on each corewise-permtable subset
                            for(i in 1:nrow(corewise_permtable)){
                              permrow <- unlist(corewise_permtable[i,])
                              
                              ### Get locations of vectors containing more than one inputs ###
                              pcurver_param <- ""
                              
                              if(length(grep("#",permrow))>0) {
                                perm_subset <- as.character(permrow[grep("#",permrow)]) 
                                # Cengiz: factors from perm are back to character
                                splitTests <- as.data.frame(unlist(strsplit(perm_subset,split='#',fixed=TRUE)))
                                F1 <- as.data.frame(permrow[!(permrow %in% perm_subset)])
                                colnames(F1) <- "Stats"
                                colnames(splitTests) <- "Stats"
                                F1 <- rbind(F1,splitTests)
                                pcurver_param <- as.character(F1[1:nrow(F1),])
                              } else {
                                pcurver_param <- as.character(permrow)
                              }
                              
                              if(i == 1){
                                pout <- as.data.frame(matrix(NA, nrow=nrow(corewise_permtable), 
                                                             ncol=length(cols), byrow=FALSE))
                              }
                              pout[i, ] <- as.character(c(pcurver(pcurver_param), as.character(permrow)))
                            }
                            return(pout)
                          }
    
    # write pcurver outvalues
    outietemp <- as.data.frame(outietemp)
    rownames(outietemp) <- as.character(idx)
    colnames(outietemp) <- cols
    
    if(loop==1){
      write.table(outietemp, file=paste0(getwd(), "/", outname, ".txt"),
                  sep="\t", append=FALSE, row.names=FALSE)
    } else if(loop>1){
      write.table(outietemp, file=paste0(getwd(), "/", outname, ".txt"),
                  sep="\t", append=TRUE, row.names=FALSE, col.names=FALSE)
    }
    
    print(paste0("pcurver ", round(idx[length(idx)]/waitlength*100, 4),
                 " percent finished: lines ", idx[length(idx)], " of ",
                 waitlength, " written. ", Sys.time()))
    
    rm(temppermtable)
    rm(outietemp)
    rm(idx)
  }
  
  gc()
  
  cat("\n")
  print(paste0("pcurver completed! ", Sys.time()))
  
  # load the output into an ff object
  if(outer_decision == 1) {
    cat("\n")
    print(paste0("Loading output into an 'ff' package R object. ", Sys.time()))
    
    outie <- read.table.ffdf(file=paste0(getwd(), "/", outname, ".txt"),
                             next.rows=waitlength, header=TRUE)
    
    assign(outname, outie, envir=.GlobalEnv)
    rm(outie)
  }
  
  # remove temporary files
  cat("\n")
  print("Removing temporary files.")
  unlink("tempfiles", recursive=TRUE)
  
  cat("\n")
  print(paste0("Process complete! ", Sys.time()))
  cat("\n")
  print(paste0("pcurver values are saved in file ", outname, ".txt"))
  if(outer_decision == 1) {
    print(paste0("pcurver values are also stored in R object ", outname))
  }
  
  invisible(stopCluster(cl))
  invisible(gc())
  
}



