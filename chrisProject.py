import statistics
import json
import pymongo
import scipy.stats as stats
import re
import copy
import pandas as pd

myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["mydatabase"]
mycol = mydb["chrisProject"]

def findPValue(test,df,value):

    ## Rmoved the brackets and split the values
    df = df.group().replace('(','').replace(')','').split(',')

    ## Needed to use PD for the types to be correct. Also made it the absolute value to ensure we get the correct pValue
    value = abs(pd.to_numeric(value.group()))


    if test == "t":
        pValue = 1 - stats.t.cdf(value,pd.to_numeric(df[0]))
    elif test == "F":
        pValue = 1 - stats.f.cdf(value,pd.to_numeric(df[0]),pd.to_numeric(df[1]))
    elif test == "z":
        pValue = 1 - stats.norm.cdf(value)
    else:
        return

    print(pValue)
    return pValue

def findValueInTxt(value,fileName):
    return readThroughTxtToFindZppr(value,fileName)


##Read in the txt file
def readThroughTxtToFindZppr(value,fileName):

    file1 = open(fileName, 'r')
    count = 0

    while True:
        count += 1
    
        # Get next line from file
        line = file1.readline()

        if count == 1:
            keys = line.split()
            keys = [x.replace('"','') for x in keys]
            print(keys)
        else:
            if count % 10000 == 0:
                print(count)

            res = dict(zip(keys[3:], [x.replace('"','') for x in line.split()[3:]]))
            res["id"] = count - 1
            

            if str(res['Zppr']) == str(value):
                return res

        # if line is empty
        # end of file is reached
        if not line:
            break
        
    
    file1.close()


def readInTxt(fileName):

    file1 = open(fileName, 'r')
    count = 0

    while True:
        count += 1
    
        # Get next line from file
        line = file1.readline()
        #print(line)

        if count == 1:
            keys = line.split()
            keys = [x.replace('"','') for x in keys]
            print(keys)
        else:
            if count % 1000 == 0:
                print(count)

            object = createObject(count,line,keys)

        # if line is empty
        # end of file is reached
        if not line:
            break
        
    
    file1.close()



def createObject(count,line,keys):

        res = dict(zip(keys[3:18], [float(x.replace('"','')) for x in line.split()[3:]]))
        res["id"] = count - 1

        #uploadToDB(res)

        return res

def uploadToDB(entry):
    
    mycol.insert_one(entry)


def generateLists(finalOutput):

    medianList = {
    "zppr" : []
    }
    
    for i,x  in enumerate(mycol.find({},{ "_id": 0})):

        #See progress every 100K entries
        if i % 10000 == 0:
            print(i)
            
        #Add each value to their list to be used to find the median
        medianList["zppr"].append(x['Zppr'])

        #Calculate min and max values and store their pValues
        if x['Zppr'] < finalOutput['zpprMin']['Zppr']:
            finalOutput['zpprMin']= x
        elif x['Zppr'] > finalOutput['zpprMax']['Zppr']:
            finalOutput['zpprMax'] = x

        
        if x['p.Zppr.half'] < .05 and x['p.Zpp33'] < .05:
            finalOutput['count1'] += 1
        elif x['p.Zppr.half'] < .05 and x['p.Zpp33'] > .05:
            finalOutput['count2'] += 1
        elif x['p.Zppr.half'] > .05 and x['p.Zpp33'] < .05:
            finalOutput['count3'] += 1
        elif x['p.Zppr.half'] > .05 and x['p.Zpp33'] > .05:
            finalOutput['count4'] += 1

        if (x['p.Zppr.half'] < .1) and (x['p.Zppr'] < .1) and x['p.Zpp33'] < .05:
            finalOutput['count5'] += 1
        elif x['p.Zppr.half'] > .1 or (x['p.Zppr'] > .1) and x['p.Zpp33'] < .05:
            finalOutput['count6'] += 1
        elif x['p.Zppr.half'] < .1 and (x['p.Zppr'] < .1) and x['p.Zpp33'] > .05:
            finalOutput['count7'] += 1
        elif x['p.Zppr.half'] > .1 or (x['p.Zppr'] > .1) and x['p.Zpp33'] > .05:
            finalOutput['count8'] += 1
    

    writeToFile(finalOutput,"T_HyperOutput.json")

    generateMMM(medianList,finalOutput)
    

def generateMMM(medianList,finalOutput):
    for x in medianList:

        finalOutput[x+'Median'] = statistics.median(medianList[x])
        
    writeToFile(finalOutput,"T_HyperOutput.json")
    #writeToFile(medianList,"T_HyperLists.json")

def writeToFile(json_object,adress):
    with open(adress, "w") as outfile:
        json.dump(json_object, outfile)

def readInJson(address = "T_Hyper.json"):
    with open(address, "r") as infile:
        data = json.load(infile)
        return data


finalOutput = {
    "zpprMin": {'Zppr': 0},
    "zpprMax": {'Zppr': 0},
    "zpprMedian": 0,
    "count1": 0,
    "count2": 0,
    "count3": 0,
    "count4": 0,
    "count5": 0,
    "count6": 0,
    "count7": 0,
    "count8": 0
}


def findMinMaxZpprInFile():
    outputFile = readInJson('T_HyperOutput.json')

    minZpprObject = findValueInTxt(outputFile['zpprMin']['Zppr'],"T_Hyper_HK.txt")
    maxZpprObject = findValueInTxt(outputFile['zpprMax']['Zppr'],"T_Hyper_HK.txt")

    outputFile['minZpprObject'] = minZpprObject
    outputFile['maxZpprObject'] = maxZpprObject

    writeToFile(outputFile,"T_HyperOutput.json")

def loopMinMaxToAddPValue():
    inputFile = readInJson('T_HyperOutput.json')

    outputFile = copy.deepcopy(inputFile)



    for key in inputFile['minZpprObject']:
        print(key)
        if key[0] == 'x':
            pValue = findPValue(inputFile['minZpprObject'][key][0],re.search("\((.*?)\)",inputFile['minZpprObject'][key]),re.search("[^=]+$",inputFile['minZpprObject'][key]))

            outputFile['minZpprObject'][key + '_pValue'] = pValue


    for key in inputFile['maxZpprObject']:
        if key[0] == 'x':
            pValue = findPValue(inputFile['maxZpprObject'][key][0],re.search("\((.*?)\)",inputFile['maxZpprObject'][key]),re.search("[^=]+$",inputFile['maxZpprObject'][key]))

            outputFile['maxZpprObject'][key + '_pValue'] = pValue

    writeToFile(outputFile,"T_HyperOutput.json")

def loopThroughPvaluesToAddToBuckets():
    
    inputFile = readInJson('T_HyperOutput.json')

    outputFile = copy.deepcopy(inputFile)

    
    buckets = {
        ".01": 0,
        ".02": 0,
        ".03": 0,
        ".04": 0,
        ".05": 0,
        "error": 0
    }


    for key in inputFile['minZpprObject']:
        

        if 'pValue' in key:

            pValue = inputFile['minZpprObject'][key]
            print(pValue)

            if pValue < .015:
                buckets[".01"] += 1
            elif .015 <= pValue < .025:
                buckets[".02"] += 1
            elif .025 <= pValue < .035:
                buckets[".03"] += 1
            elif .035 <= pValue < .045:
                buckets[".04"] += 1
            elif .045 <= pValue < .05:
                buckets[".05"] += 1
            else:
                buckets["error"] += 1
        
    outputFile['minZpprObject']["buckets"] = buckets



    buckets = {
        ".01": 0,
        ".02": 0,
        ".03": 0,
        ".04": 0,
        ".05": 0,
        "error": 0
    }


    for key in inputFile['maxZpprObject']:
        

        if 'pValue' in key:

            pValue = inputFile['maxZpprObject'][key]

            print(pValue)

            if pValue < .015:
                buckets[".01"] += 1
            elif .015 <= pValue < .025:
                buckets[".02"] += 1
            elif .025 <= pValue < .035:
                buckets[".03"] += 1
            elif .035 <= pValue < .045:
                buckets[".04"] += 1
            elif .045 <= pValue < .05:
                buckets[".05"] += 1
            else:
                buckets["error"] += 1
        
    outputFile['maxZpprObject']["buckets"] = buckets

    writeToFile(outputFile,"T_HyperOutput.json")

def zipXandXvalues ():
    inputFile = readInJson('T_HyperOutput.json')

    outputFile = copy.deepcopy(inputFile)

    newObject = {}
    xObjects = {}

    for key in inputFile['minZpprObject']:
        if key[0] != 'x':
            newObject[key] = inputFile['minZpprObject'][key]
        elif 'pValue' in key:
            if key[2] == '_':
                xObjects[key[0:2]]['pValue'] = inputFile['minZpprObject'][key]
            else:
                xObjects[key[0:3]]['pValue'] = inputFile['minZpprObject'][key]
        else:
            xObjects[key] = {}
            xObjects[key]['Test'] = inputFile['minZpprObject'][key]
            
            
    newObject['xObjects'] = xObjects
    outputFile['minZpprObject'] = newObject


    newObject = {}
    xObjects = {}

    for key in inputFile['maxZpprObject']:
        if key[0] != 'x':
            newObject[key] = inputFile['maxZpprObject'][key]
        elif 'pValue' in key:
            if key[2] == '_':
                xObjects[key[0:2]]['pValue'] = inputFile['maxZpprObject'][key]
            else:
                xObjects[key[0:3]]['pValue'] = inputFile['maxZpprObject'][key]
        else:
            xObjects[key] = {}
            xObjects[key]['Test'] = inputFile['maxZpprObject'][key]
            
            
    newObject['xObjects'] = xObjects
    outputFile['maxZpprObject'] = newObject

    writeToFile(outputFile,"T_HyperOutput.json")

zipXandXvalues()