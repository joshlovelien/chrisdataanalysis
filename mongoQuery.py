import statistics
import json
import pymongo

myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["mydatabase"]
mycol = mydb["chrisProject"]

def generateLists(finalOutput):

    medianList = {
    "zppr" : []
    }
    
    for i,x  in enumerate(mycol.find({},{ "_id": 0})):

        #See progress every 100K entries
        if i % 10000 == 0:
            print(i)
            
        #Add each value to their list to be used to find the median
        medianList["zppr"].append(x['Zppr'])

        #Calculate min and max values and store their pValues
        if x['Zppr'] < finalOutput['zpprMin']['Zppr']:
            finalOutput['zpprMin']= x
        elif x['Zppr'] > finalOutput['zpprMax']['Zppr']:
            finalOutput['zpprMax'] = x

        
        if x['p.Zppr.half'] < .05 and x['p.Zpp33'] < .05:
            finalOutput['count1'] += 1
        elif x['p.Zppr.half'] < .05 and x['p.Zpp33'] > .05:
            finalOutput['count2'] += 1
        elif x['p.Zppr.half'] > .05 and x['p.Zpp33'] < .05:
            finalOutput['count3'] += 1
        elif x['p.Zppr.half'] > .05 and x['p.Zpp33'] > .05:
            finalOutput['count4'] += 1

        if (x['p.Zppr.half'] < .1) and (x['p.Zppr'] < .1) and x['p.Zpp33'] < .05:
            finalOutput['count5'] += 1
        elif x['p.Zppr.half'] > .1 or (x['p.Zppr'] > .1) and x['p.Zpp33'] < .05:
            finalOutput['count6'] += 1
        elif x['p.Zppr.half'] < .1 and (x['p.Zppr'] < .1) and x['p.Zpp33'] > .05:
            finalOutput['count7'] += 1
        elif x['p.Zppr.half'] > .1 or (x['p.Zppr'] > .1) and x['p.Zpp33'] > .05:
            finalOutput['count8'] += 1
    

    writeToFile(finalOutput,"T_HyperOutput.json")

    generateMMM(medianList,finalOutput)
    

def generateMMM(medianList,finalOutput):
    for x in medianList:

        finalOutput[x+'Median'] = statistics.median(medianList[x])
        
    writeToFile(finalOutput,"T_HyperOutput.json")
    #writeToFile(medianList,"T_HyperLists.json")



def writeToFile(json_object,adress):
    with open(adress, "w") as outfile:
        json.dump(json_object, outfile)

def readInJson(address = "T_Hyper.json"):
    with open(address, "r") as infile:
        data = json.load(infile)
        return data


finalOutput = {
    "zpprMin": {'Zppr': 0},
    "zpprMax": {'Zppr': 0},
    "zpprMedian": 0,
    "count1": 0,
    "count2": 0,
    "count3": 0,
    "count4": 0,
    "count5": 0,
    "count6": 0,
    "count7": 0,
    "count8": 0
}

lists = generateLists(finalOutput)
